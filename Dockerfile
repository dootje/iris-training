# ARG IMAGE=store/intersystems/iris-community:2020.4.0.524.0
# ARG IMAGE=docker.iscinternal.com/intersystems/iris:2020.3.0-released
ARG IMAGE=docker.iscinternal.com/intersystems/iris:2020.1.1-latest

FROM ${IMAGE}

USER root
ENV SRC_DIR=/home/irisowner/src
COPY --chown=irisowner init/ ${SRC_DIR}/
RUN ${SRC_DIR}/root.sh

USER irisowner
RUN ${SRC_DIR}/iris.sh

HEALTHCHECK --interval=5s CMD /irisHealth.sh || exit 1
