# This script is meant to be called from Dockerfile before all others.
# It contains the shell commands to be executed as root.

echo "\nRunning ${0} as `whoami`\n"

# Standard preparations.
apt-get update -qqy
export LANG=C.UTF-8

# Install tooling we'll need later on.
apt-get install -y curl

# Create secondary journal directory.
cd /usr/irissys/mgr
printf "\nConfiguring alternate journal directory..."
sed -i "s/AlternateDirectory=.*/AlternateDirectory=\/usr\/irissys\/mgr\/journal2\//" /usr/irissys/iris.cpf
mkdir journal2
chown --reference=journal journal2

# Set timezone to UTC.
export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true
echo 'tzdata tzdata/Areas select Etc' | debconf-set-selections
echo 'tzdata tzdata/Zones/Etc select UTC' | debconf-set-selections
apt-get install -qqy --no-install-recommends tzdata

# Clean up apt stuff.
rm -rf /var/lib/apt/lists/*

# Create directories for file input and output in the training exercises.
mkdir -p /scan/in /scan/out
chmod -R 777 /scan
