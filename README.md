# Solution files for the IRIS interoperability training

Repo with answers to the ScanEmail excercises featured in the IRIS training *Developing System Integrations*.

## Setup

You can run this repo in a Docker container or in an installed IRIS instance.

### Setup for Docker

Clone this repository.
In the workspace root, run `docker-compose up -d`.

### Setup for an installed IRIS instance

~~Import the file `Installer.xml` into any namespace. *USER* will do fine.~~

### What does the setup involve?

~~For both environments, importing `Installer.xml`:

* creates an interoperability-enabled namespace *SCAN*;
* creates an empty production *Scan.EmailProduction* in it;
* creates and populates the *Employee* class in the *USER* namespace.~~

## Usage

Chapter numbers:

2 Management Portal and Productions
3 Messages
4 Business Operations
5 Business Processes
6 Business Process with Context
7 Business Services
8 Data Transformations
9 Dashboards
10 Workflow
11 Business Rules
12 Web Services - SOAP
13 Web Services - REST
14 Record Mapper

## VScode issues

* Importing or exporting of files is not supported.
  Use the Management Portal or Studio.
  There is [a GitHub issue](https://github.com/intersystems-community/vscode-objectscript/issues/79) for this.
  Meanwhile, use `docker cp`.
  
  For exporting classes and CSP files from the TRAINING namespace, issue
  `set st = ##class(Training.Util).Export(<filename>)`.
  This will export all classes Scan.* and all CSP pages into `/home/irisowner/<filename>.xml`.

* You can't open a CSP page from the editor.
  For the web form, point your browser to [http://localhost:52773/csp/training/EmailForm.csp].

* VSCode does not provide a wizard for creating new classes.
  You must type out all code, unless you define your own [code snippets](https://code.visualstudio.com/docs/editor/userdefinedsnippets).

## Docker issues

* By default, the primary and secondary journal directories coincide.
  `/irisHealth.sh` detects this and marks the container unhealthy.
  To avoid this, `init.sh` contains the lines

```bash
sed -i "s/AlternateDirectory=.*/AlternateDirectory=\/usr\/irissys\/mgr\/journal2\//" /usr/irissys/iris.cpf
mkdir journal2
chown --reference=journal journal2
```

* The IRIS Community Edition has the USER namespace Ensemble-enabled.
  However, don't use that for building the ScanEmail production.
  If would make it useless as a quasi-external ODBC source.

## Management Portal issues

* The Portal language defaults to the browser language (in my case Dutch).
  To suppress this, the container's setup runs the DisableLanguage method from the Portal.Language class.

## Data issues

* The `User.Employee` class now imports its own data automatically if they don't exist in the target namespace.
  This is done through [the Projection mechanism](https://community.intersystems.com/post/installing-cach%C3%A9-applications-using-class-projections)

* The IRIS Community Edition comes with a predefined ODBC source `User`.
  Use this to connect to the Employee database.
  For Windows you need to configure a 64-bit ODBC Data Source Name yourself.
  Make sure that it's a System DSN.

## Mail issues

We use the mail container developed by [tvial](https://hub.docker.com/r/tvial/docker-mailserver/).

```bash
docker pull tvial/docker-mailserver
cd ~/Docker
mkdir mail
cd mail
wget https://raw.githubusercontent.com/tomav/docker-mailserver/master/setup.sh
wget https://raw.githubusercontent.com/tomav/docker-mailserver/master/docker-compose.yml
wget https://raw.githubusercontent.com/tomav/docker-mailserver/master/mailserver.env
wget -O .env https://raw.githubusercontent.com/tomav/docker-mailserver/master/compose.env
chmod a+x ./setup.sh
```

Configure `.env`:

```bash
HOSTNAME=mail
DOMAINNAME=demo.mail
CONTAINER_NAME=mail
```

Configure `mailserver.env`:

```bash
POSTMASTER_ADDRESS=aldo@demo.mail
PERMIT_DOCKER=connected-networks
```

Start server:

```bash
docker-compose up -d mail
```

The script `setup.sh` is not fully compliant with the BSD-style Mac environment.
Replace the call to `readlink` with a custom function, as suggested [here](https://unix.stackexchange.com/a/534101).
Replace `${SCRIPT,,}` with `${SCRIPT}`.

Add mail account:

```bash
./setup.sh email add aldo@demo.mail demo
```

Configure Thunderbird with the new mail settings. It complains about certificates (as it should).
You'll need to overrule this.

You can now test reception of mails with a
[telnet session]([https://www.sparkpost.com/blog/how-to-check-an-smtp-connection-with-a-manual-telnet-session/).

```bash
telnet localhost 25
ehlo demo.mail
mail from: dootje@test.com
rcpt to: aldo@demo.mail
data
from: dootje@test.com
to: aldo@demo.mail
subject: Test

Test message.
.
quit
```

If the mail bounces, check `/var/log/mail/mail.log` in the mail container.
In case amavis reports a BAD_HEADER, edit `/etc/amavis/conf.d/49-docker-mailserver` to read

```conf
$final_bad_header_destiny = D_PASS;
```

Export classes with

```objectscript
set st=$system.OBJ.Export("*Production.cls,Scan.AddressFileRequest.cls,*CheckRelation*.cls,*Validate*.cls,*MakeFileOperation.cls,*GetEmployeeDetails.cls,*AddToAddressFile.cls,*CheckEmployee*.cls,*GetEmployeeInfo.cls,*EmailFileRequest.cls,*DetermineBlackList.cls,*Service.cls,/csp/training/EmailForm.csp,/csp/training/EmailSubmit.csp,*ScanFileService.cls,*BlackListCountMetric.cls,Scan-EmailScanDashboard.dashboard.DFI,*SafeEmailCountMetric.cls,Scan-SafeEmails.dashboard.DFI","/scan/Chapter9.xml")
```
